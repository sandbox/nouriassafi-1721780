<?php

/**
 * @file
 * Provider interface.
 */

/**
 * Provider interface.
 */
abstract class AffiliateLinksProvider {
  public $accid;
  public $name;
  public $provider;
  public $cloaked;
  public $fields;
  public $patterns;

  /**
   * Construct provider account.
   *
   * @param int $accid
   *   Provider account ID.
   */
  public function __construct($accid) {
    $result = db_query(
      "SELECT name, provider, cloaked, data FROM {affiliate_links_account}
      WHERE accid = :accid", array(':accid' => $accid)
    )->fetch();
    $this->accid = $accid;
    $this->name = $result->name;
    $this->provider = $result->provider;
    $this->cloaked = $result->cloaked;
    $this->data = unserialize($result->data);
    $this->fields = isset($this->data['fields']) ?
    $this->data['fields'] : array();

    $this->patterns = array();
    $result = db_query(
      "SELECT pattern FROM {affiliate_links_pattern} WHERE accid = :accid", array(':accid' => $accid)
    );
    foreach ($result as $pattern) {
      $this->patterns[] = $pattern->pattern;
    }
  }

  /**
   * Return required fields for this provider account.
   *
   * @return array
   *   An associative array with the following keys:
   *   - fields: An array of required fields. Each field can be either a simple
   *     string or an associative array with the following keys:
   *     - name: Field name.
   *     - form: An associative array of FAPI settings to override default
   *       field form.
   */
  static function getInfo() {
    return array(
      'fields' => array(
        'affiliate_id',
        'affiliate_key',
        array(
          'name' => 'merchant_id',
          'form' => array(
            '#required' => FALSE,
          ),
        ),
      ),
    );
  }

  /**
   * Parse a regular URL into its constituent parts.
   *
   * @param string $url
   *   Regular URL to parse.
   *
   * @return array
   *   Associative array containing the relevant url parts.
   */
  protected function parse_url($url) {
    $url_parts = parse_url($url);
    // Convert query string into an associative array
    if (!empty($url_parts['query'])) {
      parse_str($url_parts['query'], $url_parts['query_params']);
    }
    return $url_parts;
  }

  /**
   * Build regular URL from its constituent parts.
   *
   * @param array $url_parts
   *   Associative array containing the relevant url parts.
   *
   * @return string
   *   Regular URL built using the passed in url parts.
   */
  protected function build_url($url_parts) {
    if (!is_array($url_parts) || empty($url_parts['host'])) {
      return FALSE;
    }

    $url = !empty($url_parts['scheme']) ? $url_parts['scheme'] . '://' : 'http://';
    $url .= $url_parts['host'];
    $url .= !empty($url_parts['port']) ? ':' . $url_parts['port'] : '';

    if (!empty($url_parts['path'])) {
      $url .= (substr($url_parts['path'], 0, 1) == '/') ? $url_parts['path'] : '/' . $url_parts['path'];
    }

    if (is_array($url_parts['query_params'])) {
      $url_parts['query'] = http_build_query($url_parts['query_params']);
    }
    $url .= !empty($url_parts['query']) ? '?' . $url_parts['query'] : '';
    $url .= !empty($url_parts['fragment']) ? '#' . $url_parts['fragment'] : '';

    return $url;
  }

  /**
   * Convert a regular URL into affiliate link.
   *
   * @param string $url
   *   Regular URL to convert.
   *
   * @return string
   *   Affiliate link, cloaked if account is set to cloak link.
   */
  public function convert($url) {
    if (!empty($this->fields['affiliate_key']) && !empty($this->fields['affiliate_id'])) {
      $url_parts = $this->parse_url($url);
      $url_parts['query_params'][$this->fields['affiliate_key']] = $this->fields['affiliate_id'];
      $url = $this->build_url($url_parts);
    }
    return $url;
  }
}
