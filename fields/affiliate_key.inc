<?php

/**
 * @file
 * Affiliate Key field.
 */

/**
 * Return form element for this field.
 */
function affiliate_links_affiliate_key_form($form_state, $account = NULL,
$settings = array()) {
  $form['affiliate_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Affiliate Key'),
    '#default_value' => isset($account->fields['affiliate_key']) ?
      $account->fields['affiliate_key'] : '',
    '#required' => TRUE,
  );
  $form['affiliate_key'] = $settings + $form['affiliate_key'];
  return $form;
}

/**
 * Validate callback for affiliate_links_affiliate_key_form().
 */
function affiliate_links_affiliate_key_validate($form, $form_state,
$account = NULL) {

}
