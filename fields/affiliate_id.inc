<?php

/**
 * @file
 * Affiliate ID field.
 */

/**
 * Return form element for this field.
 */
function affiliate_links_affiliate_id_form($form_state, $account = NULL,
$settings = array()) {
  $form['affiliate_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Affiliate ID'),
    '#default_value' => isset($account->fields['affiliate_id']) ?
      $account->fields['affiliate_id'] : '',
    '#required' => TRUE,
  );
  $form['affiliate_id'] = $settings + $form['affiliate_id'];
  return $form;
}

/**
 * Validate callback for affiliate_links_affiliate_id_form().
 */
function affiliate_links_affiliate_id_validate($form, $form_state,
$account = NULL) {

}
