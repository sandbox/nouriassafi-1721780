<?php

/**
 * @file
 * Simple provider.
 */

/**
 * Simple provider.
 */
class AffiliateLinksSimple extends AffiliateLinksProvider {
  /**
   * Override AffiliateLinksProvider::getInfo().
   */
  static function getInfo() {
    return array(
      'fields' => array('affiliate_id', 'affiliate_key'),
    );
  }
}
