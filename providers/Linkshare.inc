<?php

/**
 * @file
 * Linkshare provider.
 */

/**
 * Linkshare provider.
 */
class AffiliateLinksLinkshare extends AffiliateLinksProvider {
  /**
   * Override AffiliateLinksProvider::getInfo().
   */
  static function getInfo() {
    return array(
      'fields' => array(
        'merchant_id',
        'api_key',
      ),
    );
  }

  /**
   * Override AffiliateLinksProvider::convert().
   *
   * @param string $url
   *   Regular URL to convert.
   *
   * @return string
   *   Affiliate link, cloaked if account is set to cloak link.
   */
  public function convert($url) {
    $api_key = $this->fields['api_key'];
    $merchant_id = $this->fields['merchant_id'];
    if (!empty($api_key) && !empty($merchant_id)) {
      $lookup_url = "http://getdeeplink.linksynergy.com/createcustomlink.shtml?token=$api_key&mid=$merchant_id&murl=$url";
      $response = drupal_http_request($lookup_url);
      if ($response->code == 200) {
        $new_url = $response->data;
        if (substr($new_url, 0, 7) == 'http://') {
          return $new_url;
        }
      }
    }
    return $url;
  }

}
